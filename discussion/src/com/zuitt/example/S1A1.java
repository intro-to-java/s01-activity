package com.zuitt.example;
import java.util.Scanner;
public class S1A1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String firstName;
        String lastName;
        double firstSubject;
        double secondSubject;
        double thirdSubject;

        System.out.println("Enter first name: ");
        firstName = sc.nextLine();
        System.out.println("Enter last name: ");
        lastName = sc.nextLine();
        System.out.println("Enter first subject grade: ");
        firstSubject = sc.nextDouble();
        System.out.println("Enter second subject grade: ");
        secondSubject = sc.nextDouble();
        System.out.println("Enter third subject grade: ");
        thirdSubject = sc.nextDouble();

        int avg = (int)(firstSubject + secondSubject + thirdSubject) / 3;
        System.out.println("Good day, " + firstName + " " + lastName + ".");
        System.out.println("Your grade average is: " + avg);
    }
}
